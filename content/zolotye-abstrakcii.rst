Золотые абстракции
##################
:date: 2009-12-10 15:11
:author: pilgr
:category: Заметки на полях
:tags: мысля
:slug: zolotye-abstrakcii

Какой-то бешеный ритм увеличения уровня сложности в ИТ-системах. Хочешь
быть в тонусе — поспевай за новыми технологиями, учи новое, делай новое.
Ни передыху, ни покоя тебе нету. И много ли участников этой гонки
понимают ``mov bx, 41h``? Сетований в стиле «молодежь не та» не ждите.
Об абстракциях — ждите.

О дырявых абстракциях уже многие читали. Если нет —
`рекомендую <http://local.joelonsoftware.com/wiki/Закон_Дырявых_Абстракций>`__.
Пока вы там читаете Сполски, я затяну волынку о абстракциях золотых.

Есть у людей данная Богом способность думать и творить. Но мозг в
принципе конечен, а развитие идет все более и более высоки темпами. Как
же так? Я думаю, что наш мозг замечательно оперирует абстракциями. Дайте
ему абстрагироваться, и он будет дышать полной грудью. И все потрясающее
развитие технологий — построение все более и более высокоуровневых,
более совершенных абстракций.

Вот о супермаркетах хотел сказать. Это далеко не сборщик мусора в Java.
Но также какая-никакая абстракция. Мне совершенно безразлично, какой
водитель вез мою бутылку молока. И в самом деле, почему я должен думать
об этом? Не заставляйте меня думать о том, от Буренки или от Матрены вы
выдоили его. Избавьте. Я просто прихожу и покупаю любимую бутылку
молока. И кажется так было всегда. Эх, нет. Не всегда. А когда появились
они — появились в них и покупатели. Заметьте, появились сами по себе.
Принесли свои деньги (о, очередная абстракция) и отдали их.

А электричество? Без него, говорят, и компьютеров не было бы. А вот с
какой ГЭС, АЭС или прочей \*ЭС оно поступает в мою розетку — мне ведь
нету дела. Оно бывает исчезает (здесь я снова вынужден отослать к
`Сполски <http://local.joelonsoftware.com/wiki/Закон_Дырявых_Абстракций>`__).
Но когда есть — я им пользуюсь и совершенно не задумываюсь о методах его
перераспределения в энергосетях в час пик. И создатели Mac-ов также об
этом не думали. Сейчас люди платят и за электричество и за маки.

Позвольте теперь о облачных вычислениях. Нет, у меня нету слов. Это
мега-абстракция в каждом бите ее названия. Мне кажется эта технология
даже дышит абстрактно. И хотя она еще должна доказать свое право на
существование под IT-солнцем — в нее уже текут деньги.

А SOA? Начните думать сервисами, абстрагируйтесь от каждого из ваших
монструозных приложений. И ведь работает. Если вы скептически отнеслись
к последнему предложению — не беда. Эта абстракция вами еще не изучена
как следует.

У многих программистов за пазухой есть хорошо изученная абстракция.
Например какой-то популярный фреймворк. Понимание этой абстракции
приносит хлеб с икрой программисту, и какие никакие дивиденды создателям
фреймворка. У меня есть новость. Скоро вам нужно будет учить новый
фреймворк.

Я уже не говорю о абстракциях архитектуры x86, BIOS, сборщиках мусора, и
т.д. Все они замечательно облегчают жизнь. И что самое главное —
позволяют строить еще более высокие абстракции. Ведь EJB-контейнеры и
WEB-сервера появились после Ethernet и TCP.

Каждая такая абстракция на своем уровне имеет конечную сложность и для
восприятия понятна какому-то кругу людей. Множество людей пользуются ими
не задумываясь о том что выше или ниже. Как говорится, абстракция на
абстракции сидит и абстракцией погоняет. И работающая абстракция —
полезная абстракция. На ней можно построить еще более полезную
абстракцию. Почему бы и нет?

И я пришел к выводу, что хорошая абстракция — это золотая абстракция.
Действительно, множество придуманных абстракций принесли и приносят
своим создателям неплохие проценты. А затем служат очередными витками к
росту технологий. Рынок технологий — это рынок золотых абстракций. Чем
более удачная она – тем имеет более высокую стоимость.

И еще. Все пирамиды абстракций растут не маленькими темпами. Могут ли
они рухнуть? Там выше вскользь было упоминание о абстракции денег. Вы
знаете сколько финансовых инструментов было создано поверх этой
абстракции? Мне кажется, какая-то из пирамид пошатнулась.
