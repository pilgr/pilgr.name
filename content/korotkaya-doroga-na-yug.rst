Короткая дорога на Юг
#####################
:date: 2009-12-10 15:25
:author: pilgr
:category: Путешествия
:tags: Крым, мото
:slug: korotkaya-doroga-na-yug

|image0|

Раньше я часто путешествовал на велосипеде. Начиналось все с попытки
доехать из Киева до Крыма. Затем ездил в Словакию, Венгрию, Польшу,
Турцию, Румынию, Сербию, Черногорию. Путешествие на велосипеде – это
море романтики и приключений! Но в этом году на него так и не сел, по
многим причинам. К концу лета меня посетила гениальнейшая идея –
попробовать путешествовать на мотоцикле. Идея мотопутешествия мне очень
понравилась, и я с великим удовольствием мучился выбором мотоцикла,
учился в мотошколе, получал права, покупал мотоцикл и всячески готовился
к поездке. И снова, как впервые на велосипеде, из Киева – в Крым!
Поехали!

Путешествие я назвал «Короткая дорога на Юг». Ведь чтобы доехать на
велосипедах нам нужно было 6 дней. А на простеньком китайском мотоцикле
хватило всего двух.

Новенький `Keway Speed
150 <http://www.keeway.com.ua/motorcycle/speed.html>`__, купленный за
`смешные деньги <http://www.keeway.com.ua/sale.html>`__, был гружен
всяким дорожным скарбом под завязку. Но и неподготовленной девушке
удержать его не составляло труда. Мотоцикл очень легкий.

|image1|

А два скрученных коврика напоминали реактивные турбины и придавали
солидности :).

|image2|

В Севастополь приехали без проблем. Раз в час приходилось
останавливаться, чтобы размяться. В Крыму в середине сентября пустынно и
тихо. На набережной Севастополя все продавцы сидят со скучающими лицами
и только то и говорят, что о закрытии сезона.

|image3|

Затем мы проехали по южному побережью до Феодосии. Ночевали по большей
части в палатке, часто у кромки моря или хорошего озера. Спится под шум
прибоя просто отменно!

|image4|

|image5|

В Малореченском.

|image6|

Макеты кораблей на колоннах.

|image7|

Под церковью находится музей морских катастроф. Музей хорош, стоит
посещения. Наверняка вы не знаете, что при гибели «Армении» в Черном
море от немецкой торпеды погибло намного больше людей, чем при гибели
Титаника. На каждую большую катастрофу показывается ролик в
соответствующей обстановке.

Ближе к Феодосии погода начала напоминать, что все-таки на дворе осень.
В конце концов пошел дождь.

|image8|

В обратный путь из Крыма я хотел проехать через Арабатскую стрелку. Это
узкая полоска степи, где на протяжении более 100 километров тянется по
левую сторону озеро Сиваш, а по правую – Азовское море. Мечта проехать
его на велосипеде так и канула в лету, зато осуществилась на мотоцикле.
Кстати, встретили там одного велосипедиста.

|image9|

Через стрелку идут множество грунтовых дорог, но все в одном
направлении, заблудиться не возможно. 90 километров грунтовки подобны
жесткой стиральной доске.

|image10|

Изредка встречаются автомобили. Встречный Lanos сильно обрадовался, что
ему сообщили, что осталось всего 30 км. Убеждал дальше не ехать, мол,
дорога еще хуже.

На берегу Сиваша умельцы чем-то промышляют.

|image11|

На самой стрелке встречаются изредка одинокие здания со следами жизни.

|image12|

Встречается и живность пернатая.

|image13|

Полоса стрелки достаточно широкая, так что на ней есть и озера
потрясающе синего цвета.

|image14|

Кстати я попробовал воду в Сиваше, на вкус как в море. А говорят,
концентрация соли должна быть побольше.

|image15|

На другой стороне штормило Азовское море.

|image16|

Искушения покупаться не возникало. Пляжи абсолютно пустынные.

|image17|

Наконец-то грунтовка осталась позади.

|image18|

На следующий день мы уже были в Одессе.

|image19|

Где чайки ждали подаяния.

|image20|

И готовы ради него делать были всевозможные пируэты, ловя кусочки хлеба
прямо в воздухе над нашими головами.

|image21|

|image22|

|image23|

Но потом им надоело, и они разлетелись.

|image24|

Один раз нас остановили гаишники за нарушение – не правильно выехал на
трассу, через сплошную линию. Подходит, спрашивает документы.
Интересуется мотоциклом.
|  – Китаец?
|  – Китаец…
|  И все в таком-же роде. Осматривает его дальше, и тут начинает
смеяться! Подзывает напарника и начинают громко смеяться вместе.

|image25|

Увидели бедного кузнечика, которого ночью где-то на скорости заклинило
под отражателем.

|image26|

На том посмеялись, и даже не смотрели документов – отпустили :).

Знаете, путешествовать на мотоцикле – потрясающе интересно!

Для мотосочувствующих.

Расход на этом 150-кубовом мотоцикле составил 2,86 л./100 км.
Было одно падение на серпантине, при спуске с Ай-Петри после дождя.
Слава Богу, все обошлось только глиной на боковой сумке.
Крейсерская скорость – 80-90 км/час. Больше не едет.
Один раз закипел и пропал задний тормоз, при спуске. После этого начал более активно тормозить передачами.
За все время открутилось пару болтиков от вибрации, и тросик
спидометра от приборной панели. А так с мотоциклом больше не было
никаких проблем.
Сидеть что водителю, что пассажиру не очень то и комфортно. Каждый
час приходилось делать остановки, чтобы размяться.
Всего пройдено около 2500 км.

.. |image0| image:: http://lh5.ggpht.com/_LVSJuwsqlLU/Srp7aI9mddI/AAAAAAAAAWE/hEheXY8T05E/s800/IMG_1514.JPG
.. |image1| image:: http://lh3.ggpht.com/_LVSJuwsqlLU/Srp7byClovI/AAAAAAAAAWI/kERrI2NvMIM/s800/IMG_1536.JPG
.. |image2| image:: http://lh5.ggpht.com/_LVSJuwsqlLU/Srp7eMqqZKI/AAAAAAAAAWM/6Dq3hmS7l-s/s800/IMG_1515.JPG
.. |image3| image:: http://lh4.ggpht.com/_LVSJuwsqlLU/Srp7fPSVlOI/AAAAAAAAAWQ/JkmkI4vCpRc/s800/IMG_1502.JPG
.. |image4| image:: http://lh4.ggpht.com/_LVSJuwsqlLU/Srp7iZyUbpI/AAAAAAAAAWY/EujTVMmjF_0/s800/IMG_1513.JPG
.. |image5| image:: http://lh6.ggpht.com/_LVSJuwsqlLU/Srp8jvZNCiI/AAAAAAAAAWc/lPt1N2nWKN0/s800/IMG_1609.JPG
.. |image6| image:: http://lh4.ggpht.com/_LVSJuwsqlLU/Srp8pVrgl7I/AAAAAAAAAWk/xhMLEIp-tsc/s800/IMG_1533.JPG
.. |image7| image:: http://lh5.ggpht.com/_LVSJuwsqlLU/Srp8swK_ZAI/AAAAAAAAAWo/btb2yQdcEKo/s800/IMG_1537.JPG
.. |image8| image:: http://lh4.ggpht.com/_LVSJuwsqlLU/Srp8yoBz23I/AAAAAAAAAWs/kUORnx9YktU/s800/IMG_1553.JPG
.. |image9| image:: http://lh3.ggpht.com/_LVSJuwsqlLU/Srp81xOVlXI/AAAAAAAAAWw/ffjgj45WOIs/s800/IMG_1573.JPG
.. |image10| image:: http://lh5.ggpht.com/_LVSJuwsqlLU/Srp89xnbLBI/AAAAAAAAAW4/JucDHaS-q8c/s800/IMG_1576.JPG
.. |image11| image:: http://lh6.ggpht.com/_LVSJuwsqlLU/Srp85Lx_KQI/AAAAAAAAAW0/Ib_3jTcWx2o/s800/IMG_1574.JPG
.. |image12| image:: http://lh6.ggpht.com/_LVSJuwsqlLU/Srp9BDJTb4I/AAAAAAAAAW8/uWzd8aSEyA4/s800/IMG_1579.JPG
.. |image13| image:: http://lh4.ggpht.com/_LVSJuwsqlLU/Srp9FiFb4cI/AAAAAAAAAXA/FipA_Loz91o/s800/IMG_1580.JPG
.. |image14| image:: http://lh6.ggpht.com/_LVSJuwsqlLU/Srp9NjbRfsI/AAAAAAAAAXI/nbUxCkOSnso/s800/IMG_1589.JPG
.. |image15| image:: http://lh5.ggpht.com/_LVSJuwsqlLU/Srp9IsAsb2I/AAAAAAAAAXE/JWPFL1q2ka0/s800/IMG_1585.JPG
.. |image16| image:: http://lh4.ggpht.com/_LVSJuwsqlLU/Srp9Tb28cVI/AAAAAAAAAXM/WpYD2o1lKuw/s800/IMG_1594.JPG
.. |image17| image:: http://lh3.ggpht.com/_LVSJuwsqlLU/Srp9XhdCX9I/AAAAAAAAAXQ/JJTH3nsXdOQ/s800/IMG_1595.JPG
.. |image18| image:: http://lh5.ggpht.com/_LVSJuwsqlLU/Srp9cppY81I/AAAAAAAAAXU/Lmd_2Jrv2D0/s800/IMG_1603.JPG
.. |image19| image:: http://lh4.ggpht.com/_LVSJuwsqlLU/Srp9gOIdOsI/AAAAAAAAAXY/8YdLP1r488k/s800/IMG_1625.JPG
.. |image20| image:: http://lh3.ggpht.com/_LVSJuwsqlLU/Srp9kSYz5bI/AAAAAAAAAXc/FWYcuLgc4yE/s800/IMG_1639.JPG
.. |image21| image:: http://lh4.ggpht.com/_LVSJuwsqlLU/Srp9nnUbzjI/AAAAAAAAAXg/ti25AJjfmBU/s800/IMG_1640.JPG
.. |image22| image:: http://lh4.ggpht.com/_LVSJuwsqlLU/Srp9z73ynwI/AAAAAAAAAX4/qh8EydRpepI/s800/IMG_1642.JPG
.. |image23| image:: http://lh5.ggpht.com/_LVSJuwsqlLU/Srp94xJfeTI/AAAAAAAAAYA/PvMZOO33kNo/s800/IMG_1645.JPG
.. |image24| image:: http://lh5.ggpht.com/_LVSJuwsqlLU/Srp9-pwQ0fI/AAAAAAAAAYE/ns4QZUboq0Q/s800/IMG_1656.JPG
.. |image25| image:: http://lh5.ggpht.com/_LVSJuwsqlLU/Srp-HeiFgvI/AAAAAAAAAYM/Sn1izRZfSGw/s800/IMG_1657.JPG
.. |image26| image:: http://lh3.ggpht.com/_LVSJuwsqlLU/Srp-MffrHSI/AAAAAAAAAYQ/yz7qVv1rJIw/s800/IMG_1660.JPG
