H2 — приятная встраиваемая СУБД
###############################
:date: 2009-12-11 16:27
:author: pilgr
:category: Заметки на полях
:tags: H2, базы данных
:slug: h2-priyatnaya-vstraivaemaya-subd

|image0|

.. raw:: html

   <div>

|H2logo|

.. raw:: html

   </div>

.. raw:: html

   <div>

Да — очень приятная.

.. raw:: html

   </div>

Я искал СУБД, которую можно было бы без большого труда встроить в
небольшое веб-приложение на Java. На одном из форумов встретил
упоминание о `H2 <http://h2database.com/html/main.html>`__
(`wiki <http://ru.wikipedia.org/wiki/H2>`__). До того момента о такой
СУБД я не слышал и ради интереса решил на нее посмотреть. Оказалось, что
она предельно просто встраивается в любое java-приложение, обладает
собственной веб-консолью с автодополнением кода, прекрасной
справкой-подсказкой, многими интересными фичами и при этом упакована в
крохотную библиотеку без единой дополнительной зависимости. С тех пор я
использую H2 в любом проекте, где нету каких-то уж очень особых
требований к СУБД.

|H2|

Как встроить H2 в ваше приложение?
----------------------------------

Очень просто! Если вы используете Maven2, добавьте новую заисимость в файл *pom.xml*.

.. code-block:: xml

  com.h2database
  h2
  1.2.125

Без использования Maven нужно скачать библиотеку и вручную добавить ее к своему проекту.
Установить соединение со встроенной БД можно так:

.. code-block:: java

   private final String JDBC_DRIVER_NAME = "org.h2.Driver";
   private final String JDBC_URL ="jdbc:h2:workspace/soaboard/soaboard";
   private final String JDBC_USER = "sa";
   private final String JDBC_PASSWORD = "";

   Connection getConnection() throws SQLException, ClassNotFoundException{
      if (connection == null){
         Class.forName(JDBC_DRIVER_NAME);
         connection = DriverManager.getConnection(JDBC_URL, JDBC_USER,JDBC_PASSWORD);
      }
      return connection;
   }

Да, это все.

Обратите внимание на переменную *JDBC_URL*. В примере происходит
соединение со встроенной в приложение БД, у которой файл данных
*soaboard* расположен по пути *workspace/soaboard/* относительно текущей
директории. Можно указать и полный путь к файлу. В любом случае, если
его не существует к моменту запуска приложения — СУБД автоматически его
создаст.

Кстати, я использую Eclipse, поэтому текущей директорией для запущенного
web-приложения является каталог с установленным Eclipse. Таким образом
сам файл БД входит в состав проекта и его легко синхронизировать вместе
с проектом через систему контроля версий.

Как добавить web-консоль H2 к вашему приложению?
------------------------------------------------

Дело в том, что когда H2 работает во встроенном режиме, доступ к файлу
данных возможен только изнутри того-же экземпляра JVM, который запустил
процесс СУБД. Но есть простой и красивый выход. Можно добавить
web-консоль H2 к своему проекту. Для этого достаточно всего лишь
вставить пару секций в файл *web.xml* вашего проекта. Вот такие:

.. code-block:: xml

  H2Console
  org.h2.server.web.WebServlet
  1

  H2Console
  /console/\*

Конечно, это все.

Запустите свой проект и перейдите по адресу
*htpp://ваш_проект/console*. Доступ к встроенной БД можно теперь
получить прямо во время работы основного приложения через вот такую
симпатичную консоль:

 |image3|

Во время набора SQL-кода предлагается авто-завершение и запоминается
история запросов, так что вернуться к предыдущим запросам не составит
труда:

|H2console|

И даже если SQL синтаксис в памяти подзабылся, всегда можно сходить и
`подсмотреть <http://h2database.com/html/grammar.html>`__ искомое
выражение.

Также H2 прекрасно работает как отдельный сервер баз данных и в режиме
"БД в памяти". Поддерживает шифрование файла данных и много многое
другое. Посмотрите на нее, хорошо?
`Она <http://h2database.com/html/main.html>`__ достойна вашего внимания.

На заглавном фото — Черногория.

.. |image0| image:: http://lh3.ggpht.com/_LVSJuwsqlLU/SfNYfoL9TKI/AAAAAAAAAGo/5nvvGQvJu54/s800/IMG_0418.JPG
.. |H2logo| image:: http://h2database.com/html/images/h2-logo.png
   :target: http://h2database.com/
.. |H2| image:: /uploads/2009/12/H2.png
.. |image3| image:: /uploads/2009/12/H2_console_start.PNG
.. |H2console| image:: /uploads/2009/12/H2_console.PNG
