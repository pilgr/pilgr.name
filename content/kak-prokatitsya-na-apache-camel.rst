Как прокатиться на Apache Camel?
################################
:date: 2009-12-25 11:18
:author: pilgr
:category: Интеграция
:tags: Apache Camel, EIP, framework, integration-review
:slug: kak-prokatitsya-na-apache-camel

|image0|

|image1|

`Apache Camel <http://ru.wikipedia.org/wiki/Apache_Camel>`__ — простой,
замечательный Java-framework, который в первую очередь предназначен для
интеграции приложений и создания интеграционных решений. Связать пару
баз данных, наладить импорт из файлов в унаследованное приложение,
сообщать о новых заказах в Jabber-клиент, обернуть веб-сервисами
какой-то "хлам", стать основой для композитного приложения на базе 10-ка
существующих унаследованный корпоративных приложений — это его стихия.
Как минимум, о нем нужно знать уже сейчас, чтобы в нужный момент просто
начать использовать для решения своих насущных задач. Разбор Верблюда и
изучение первого проекта на его основе смотрите в новой статье от
integration-review.com — `"Apache Camel — первая попытка прокатиться на
Верблюде" <http://integration-review.com/apache-camel-first-ride>`__.

На заглавном просто-фото — вездеход с пассажиром в пустыне Синайского
полуострова. Фото автора

.. |image0| image:: http://lh3.ggpht.com/_LVSJuwsqlLU/SzPbPF4f73I/AAAAAAAAAfg/Ol3HdgIn8zk/s800/IMG_0016.JPG
.. |image1| image:: http://camel.apache.org/banner.data/apache-camel-6.png
