Green Sweden #1
###############
:date: 2013-08-19 23:28
:author: pilgr
:category: Green Sweden
:tags: green sweden, podcast
:slug: green-sweden-1

Первый и пилотный выпуск подкаста Green Sweden.

`Скачать </uploads/2013/08/green-sweden-1-mastered.mp3>`__

-  Где, кто, что и почему
-  Эриксон и Андерсон
-  Про пин-код и английский
-  Признаюсь в страшном
-  Одинаковые люди
-  Как я велосипед покупал
-  Капля цен

