JBI (Java Business Integration). Основы и перспективы.
######################################################
:date: 2009-12-10 15:42
:author: pilgr
:category: Интеграция
:tags: jbi
:slug: jbi-java-business-integration-osnovy-i-perspektivy

На `integration-review.com <http://integration-review.com>`__ появилась
новая статья. На этот раз речь идет о JBI — как самой спецификации так и
продуктах на ее основе.

Идея создания спецификации не родилась на пустом месте. В 2003 году
вокруг SOA уже было не мало маркетингового шума и компания Sun решила
застолбить себе место в мире современной бизнес-интеграции. Поэтому в
августе 2005 г. в рамках Java Community Process (JCP) под номером
JSR-208 мир увидел окончательную, первую редакцию спецификации Java
Business Integration (JBI). `Далеe
→ <http://integration-review.com/jbi-overview>`__
