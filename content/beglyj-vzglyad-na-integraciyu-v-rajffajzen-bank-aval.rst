Беглый взгляд на интеграцию в Райффайзен Банк Аваль
###################################################
:date: 2009-12-15 17:12
:author: pilgr
:category: Интеграция
:tags: WebSphere Message Broker, аваль, банки
:slug: beglyj-vzglyad-na-integraciyu-v-rajffajzen-bank-aval

|image0|

`Райффайзен Банк Аваль <http://aval.ua>`__ — один из крупнейших банков
Украины с большим количеством филиалов, бизнес-направлений и
сотрудников. И, конечно же, своей историей развития. Как это аукнулось
софтовой части IT?

Сейчас на пользу банка работают 100+ приложений. Да, больше сотни. Из
них только 3 различных core systems — ОДБ (ОперДень Банка). Одна из них
в нескольких десятках экземпляров. Множество самописных приложений. И
как же это все работает слажено?

Интеграцией в банке занимается целое управление. Больше года назад в
качестве корпоративного стандарта для интеграции приложений был выбран
`WebSphere Message
Broker <http://www-01.ibm.com/software/integration/wbimessagebroker/features/>`__.
Процесс построен так, что все интеграционные сценарии с учетом
реализации на Message Broker пишут внутренние аналитики под бизнес
требования.  Непосредственно же разработка и поддержка написанного
отдана на аутсорс. Тестирование отдано на откуп внутреннему QA отделу.
Также существует группа, которая занимается мониторингом всех
short-running процессов и отслеживает инциденты.

А в вашей компании как налажены процессы интеграции?

На заглавном просто-фото — просьба о свободном проезде, в Черногории.
Фото автора

.. |image0| image:: http://lh5.ggpht.com/_LVSJuwsqlLU/Sku4KodHRbI/AAAAAAAAAQw/u50IOZPj61E/s800/IMG_0444.JPG
