Сервис отправки SMS как веб-сервис
##################################
:date: 2010-01-20 17:05
:author: pilgr
:category: Интеграция
:tags: Apache Camel, sms, web-service
:slug: servis-otpravki-sms-kak-veb-servis

У Ericsson есть замечательное подразделение — `Ericsson
Labs <https://labs.ericsson.com/>`__. Среди доступных у лаборатории
сервисов обнаружился и сервис для `отправки/приема
sms-сообщений <https://labs.ericsson.com/apis/sms-send-and-receive/>`__.
Чтобы им воспользоваться, нужно зарегистрироваться, получить API-ключик
и можно отослать 1000 sms-сообщений абсолютно бесплатно.

Чтобы отправить смс, достаточно послать запрос:

``http://sms.labs.ericsson.net/send?key=[YOUR_KEY_HERE]&to=[MSISDN]&message=This is a message``

Предоставляется и обратный интерфейс. Достаточно зарегистрировать свой
ID и указать callback-ссылку. Сообщения нужно отправлять на шведский
номер +46 73 7494050. Много говорить не буду, все описано в
`документации <https://labs.ericsson.com/apis/sms-send-and-receive/documentation>`__.

Такой сервис очень удобно использовать в `Apache
Camel <http://ru.wikipedia.org/wiki/Apache_Camel>`__. Например, для
уведомления о новых заказах или важных ошибках. Вот так будет выглядеть
маршрут в Camel, который отправит мне sms-сообщение:

``from("direct:newFailureEvent").to("http://sms.labs.ericsson.net/send?key=[ваш_API_ключик]&to=+380934805410&message=I'm reading pilgr.name");``

Прекрасный сервис, к тому-же в Apache Camel использовать его проще
простого!
