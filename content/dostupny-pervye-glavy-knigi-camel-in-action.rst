Доступны первые главы книги Camel in Action
###########################################
:date: 2009-12-10 15:47
:author: pilgr
:category: Интеграция
:tags: Apache Camel, книги
:slug: dostupny-pervye-glavy-knigi-camel-in-action

`Claus Ibsen <http://davsclaus.blogspot.com/>`__ в соавторстве с
`Jonathan Anstey <http://janstey.blogspot.com/>`__ и Hadrian Zbarcea
сейчас пишет хорошую и нужную книгу о `Apache
Camel <http://camel.apache.org/>`__ — `Camel in
Action <http://www.manning.com/ibsen/>`__.

.. raw:: html

   <p>

.. raw:: html

   <center>

|image0|

.. raw:: html

   </center>

.. raw:: html

   </p>

Первая глава, которая знакомит с Apache Camel, уже
`доступна <http://www.manning.com/ibsen/ch01_zbarcea_ibsen.pdf>`__
свободно. А черновики остальных можно получить за $27.50.

Пару слов о самом Apache Camel. Это гибкий, мощный, не сложный и просто
прекрасный фреймфорк для создания разнообразных интеграционных решений и
композитных приложений c использованием `шаблонов интеграции
корпоративных приложений <http://integration-review.com/books>`__.
Кстати, недавно вышел
`релиз <http://camel.apache.org/camel-200-release.html>`__ версии 2.0.0.

.. |image0| image:: http://www.manning.com/ibsen/ibsen_cover150.jpg
