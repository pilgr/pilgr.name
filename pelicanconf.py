#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = u'Alex Masny'
SITENAME = u'pi/\\gr.n/\\me'
SITEURL = 'http://pilgr.name'

PATH = 'content'

TIMEZONE = 'Europe/Paris'

DEFAULT_LANG = u'ru'

DEFAULT_PAGINATION = 10

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True

STATIC_PATHS = ['images', 'uploads']

FEED_RSS = 'rss.xml'
CATEGORY_FEED_RSS = '%s/rss.xml'

DELETE_OUTPUT_DIRECTORY = True

THEME = "themes/chunk"

DISPLAY_CATEGORIES_ON_MENU = False
DISPLAY_PAGES_ON_MENU = False
MENUITEMS = (
    ('About', '/about.html'),
    ('Green Sweden', '/category/green-sweden.html'),
    ('Android', '/category/android.html'),
)

SITEURL_HOME = '/'
SITESUBTITLE = 'Так сложилось исторически'
SINGLE_AUTHOR = True
GOOGLE_ANALYTICS = 'UA-8499033-3'
DISQUS_SITENAME = 'pilgr.name'

